export ID="/subscriptions/b4a1330d-d402-4692-8976-3ce9ad088412/resourcegroups/bkkrg/providers/microsoft.compute/virtualmachines/bkk"
az vm extension set \
--name AzureMonitorLinuxAgent \
--publisher Microsoft.Azure.Monitor \
--ids $ID \
--enable-auto-upgrade true
