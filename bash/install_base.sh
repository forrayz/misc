fimi manta betaflight
modprob usbserial vendor=1209 product=5741


#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
Black='\033[0;30m' ;Red='\033[0;31m';Green='\033[0;32m';Brown_Orange='\033[0;33m';Blue='\033[0;34m';Purple='\033[0;35m';Cyan='\033[0;36m'
LightGray='\033[0;37m';DarkGray='\033[1;30m';LightGreen='\033[1;32m';Yellow='\033[1;33m';LightBlue='\033[1;34m';LightPurple='\033[1;35m'
LightCyan='\033[1;36m'
White='\033[1;37m'

echo -e "${LightGray}╔════════════════════════════════════════════════════════════╗"
echo -e "${LightGray}║     install_base.sh BASH scipt                             ║"
echo -e "${LightGray}║     Author        : Zoltán Forray                          ║"
echo -e "${LightGray}║     version 1.0.2 :                                        ║"
echo -e "${LightGray}║                                                            ║"
echo -e "${LightGray}║                                                            ║"
echo -e "${LightGray}╚════════════════════════════════════════════════════════════╝"

echo -e "${White}argument═1═════════════════>$1"
#trap "set +x; sleep 1; set -x" DEBUG

# alap programok pi
apt install git fish mc vim openssh-server rsync gdebi-core chrome-gnome-shell \
sshfs arandr meld gnome-tweaks vlc virt-manager ubuntu-restricted-extras curl ansible-lint \
cowsay fortune figlet ruby default-jdk tree gnome-shell-extensions-gpaste mysql-client libfuse2 \
terminator transgui elisa gnome-screenshot whois haruna fbreader members read-edid tmate codelite traceroute
sudo gem install lolcat

# alap programok
sudo apt install git fish mc vim openssh-server rsync gdebi-core chrome-gnome-shell \
sshfs arandr meld gnome-tweaks vlc virt-manager curl ansible-lint \
cowsay fortune figlet ruby default-jdk tree gnome-shell-extensions-gpaste mysql-client libfuse2 \
terminator transgui elisa gnome-screenshot whois haruna fbreader members read-edid tmate codelite traceroute
sudo gem install lolcat

# git beálljtása
git config --global credential.helper 'store --file /home/$USER/.git-credentials'
# om my fish / bash
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"


# ásbrú
curl -1sLf 'https://dl.cloudsmith.io/public/asbru-cm/release/cfg/setup/bash.deb.sh' | sudo -E bash ;\
sudo apt install asbru-cm

# azure powersh
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add - ;\
sudo curl -o /etc/apt/sources.list.d/microsoft.list https://packages.microsoft.com/config/ubuntu/18.04/prod.list ;\
sudo apt-get update ;\
sudo apt-get install -y powershell ;\
pwsh --version


# docker és docker-compose
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

#docker on u22
apt install -y ca-certificates curl gnupg lsb-release -y ; \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg ; \
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null ;\
sudo apt-get update -y ; sudo apt install docker-ce docker-ce-cli containerd.io -y ; \
newgrp docker ; usermod -aG docker $USER

 ;\

sudo useradd -m -d /home/forrayz forrayz ; sudo usermod -aG docker forrayz ; sudo usermod -aG wheel forrayz
sudo useradd -m -d /home/jenovaim jenovaim ; sudo usermod -aG docker jenovaim ; sudo usermod -aG sudo jenivaim
sudo useradd -m -d /home/bitbucket-runner bitbucket-runner ; sudo usermod -aG docker bitbucket-runner

set usrname="forray.arnita" ; sudo useradd --shell /usr/bin/bash --comment "Forray Arnita" -m -G vboxusers -d /home/$usrname $usrname; sudo passwd $usrname
set usrname="forray.andras" ; sudo useradd --shell /usr/bin/bash --comment "Forray András" -m -G vboxusers -d /home/$usrname $usrname; sudo passwd $usrname


usrname="forray.andras" ; useradd --comment "Forray András" -m -G vboxusers -d /home/$usrname ; passwd $usrname



sudo curl -L "https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose ;\
sudo chmod +x /usr/local/bin/docker-compose

sudo curl -L "https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose2 ;\
sudo chmod +x /usr/local/bin/docker-compose2


docker run --rm -it --name opensuse-sose opensuse/leap:15.1 bash
docker run --rm -it --name centos7 centos7 bash
docker run --rm -it --name debian debian:stretch-slim bash
docker run --rm -it --name oraclelinux8 oraclelinux:8.4 bash
podman run --rm -it  docker.io/jarylc/tgenv-tfenv-alpine bash

podman run --rm -it  atlassian/default-image:3 bash

docker run --rm -it --name bamboo atlassian/bambooserver
atlassian/default-image:3
# docker nuke
docker-compose rm -f -s -v ;docker system prune -a
  docker-compose -p fikszalt_docker --file docker-stacks/fikszalt/docker-compose.yaml rm -f -s -v ;docker system prune -a



  ##### 5.1 erase all stuff (bash/fish)
  ```bash
  docker stop $(docker ps -aq) ; sudo docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs sudo docker rm ; docker rmi $(docker images -q); docker volume prune
  ```
  ```fish
  docker stop (docker ps -aq) ; docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm ; docker rmi (docker images -q); docker system prune --force --all
  podman stop (podman ps -aq) ; podman ps -a | grep Exit | cut -d ' ' -f 1 | xargs podman rm ; podman rmi (podman images -q); podman system prune --force --all

  ```

# osx alatt ubuntu
cd .. ; mkdir VM
qemu-img create -f qcow2 VM/ubuntu22.qcow2 60G
