!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

echo  ╔════════════════════════════════════════════════════════════╗"
echo  ║     install_base.sh BASH scipt                             ║"
echo  ║     Author        : Zoltán Forray                          ║"
echo  ║     version 1.0.2 :                                        ║"
echo  ║                                                            ║"
echo  ║                                                            ║"
echo  ╚════════════════════════════════════════════════════════════╝"

echo "argument1═════════════════>$1"
#trap "set +x; sleep 1; set -x" DEBUG