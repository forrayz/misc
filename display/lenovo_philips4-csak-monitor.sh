#!/bin/sh
xrandr --newmode "2560x1440_55.00"  284.00  2560 2744 3016 3472  1440 1443 1448 1489 -hsync +vsync
xrandr --addmode HDMI-1-1 2560x1440_55.00
xrandr --output eDP-1-1 --off --output DP-1-1 --off --output HDMI-1-1 --primary --mode 2560x1440_55.00 --pos 0x0 --rotate normal
