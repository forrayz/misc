#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
# 2560x1440 54.93 Hz (CVT) hsync: 81.80 kHz; pclk: 284.00 MHz
# Modeline "2560x1440_55.00"  284.00  2560 2744 3016 3472  1440 1443 1448 1489 -hsync +vsync

sudo xrandr --newmode "2560x1440_55.00"  284.00  2560 2744 3016 3472  1440 1443 1448 1489 -hsync +vsync
sudo xrandr --addmode XWAYLAND1 "2560x1440_55.00"

xrandr --output XWAYLAND1 --primary --mode 2560x1440_55.00 --pos 1920x0 --rotate normal  --output XWAYLAND0 --mode 1920x1080 --pos 0x0 --rotate normal

#sudo systemd-resolve --set-dns=172.20.1.100 --set-dns=172.20.1.140 --interface=tun0 ; \
#sudo systemd-resolve --set-domain=ulyssys.hu --set-domain=si.net --set-domain=alig.hu --interface=tun0

#sudo resolvectl domain tun0 alig.hu si.net ulyssys.hu
#sudo resolvectl dns tun0 172.20.1.100 172.20.1.140
#sudo resolvectl --interface tun0

