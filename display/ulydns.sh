#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
echo  "╔════════════════════════════════════════════════════════════╗"
echo  "║     set_uly_dns.sh BASH scipt                              ║"
echo  "║     Author        : Zoltán Forray                          ║"
echo  "║     version 1.0.2 :                                        ║"
echo  "║     As dns specifications not pushed by the VPN we do      ║"
echo  "║     at our side...                                         ║"
echo  "╚════════════════════════════════════════════════════════════╝"
# trap "set +x; sleep 1; set -x" DEBUG

DNS1=172.20.1.100
DNS2=172.20.1.140

 if hash systemd-resolve 2>/dev/null 
    then
        sudo systemd-resolve --set-dns=$DNS1 --set-dns=$DNS2 --interface=tun0
        sudo systemd-resolve --set-domain=ulyssys.hu --set-domain=si.net --set-domain=alig.hu --interface=tun0
    else
        echo "command systemd-resolve does not exist ...sorry about it..."
    fi

if hash resolvectl 2>/dev/null
    then
        sudo resolvectl domain tun0 alig.hu si.net ulyssys.hu
        sudo resolvectl dns tun0 $DNS1 $DNS2
    else
        echo "command systemd-resolve does not exist ...sorry about it..."
    fi