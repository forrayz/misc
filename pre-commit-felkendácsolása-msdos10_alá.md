```bash
1. Telepítsd ezt https://gitforwindows.org/
2. Nyiss egy powershell-t admin módban és add ki ezt a parancsot : Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
3. Nyiss egy GitBash-t admin módban és add ki ezt a parancsot: choco instal make
4. Telepitsd a python3 at innen : https://www.python.org/ftp/python/3.11.9/python-3.11.9-amd64.exe
5. Futtasd ezt a parancsot git bash ból: pip install pre-commit
6. innentől minden projecten ha kiadod ezt a parancsot :make akkor a pre-commit ot beüzemeli a make parancs.
	Esetlegesen előfordulhat hogy figyelmeztet hogy egyes linterek -et neked kel telepitened de ezek telepítése máregy fejlesztőnek nem lehet akadály mert jellemzően az adot programpzási nyelvhez vannak.